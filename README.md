# Lovell development guide

## Installation of release

So assuming that you have created a folder for your installation and you are alread logged in to lxplus/plus:

First thing to do is:
```
lb-dev Lovell/vXrY
```

Wheres vXrY is the version that you want to install. (e.g. v3r0)

```
cd  ./LovellDev_vXrY
git lb-use Lovell
git lb-checkout Lovell/vXrY LovellSys
git lb-checkout Lovell/vXrY VeloAnalysisFramework
git lb-checkout Lovell/vXrY VeloOfflineGUI
git lb-checkout Lovell/vXrY VeloWebGUI
```

and now you can `make` the project.
This installation allows you to run git lb-scripts.


## Installation for development

```
git clone ssh://git@gitlab.cern.ch:7999/lhcb/Lovell.git
cd Lovell
lb-project-init
make
```

This kind of installation provides you with pure git, and does not allow you to use git lb-scripts.
This also enables you to use branches


# Running lovell on your local machine

after building the project run

```
./build.$CMTCONFIG/run lovell --run-data-dir=/afs/cern.ch/work/m/mmajewsk/public/VetraOutputCrocombe --iv-data-dir=/afs/cern.ch/work/a/acrocomb/public/IVScan --calibration-data-dir=/afs/cern.ch/work/m/mmajewsk/public/calib_data/
```

## Running Tell1 parameters download
1. first you have to run the script that downloads the parameters as xml files
```
lb-run --runtime LHCb/v42r2 Vetra/v17r0 python VeloAnalysisFramework/scripts/veloconddb_dumper.py /path/to/xml_destination/folder
```
2. Next you have to convert xml files to root
```
lb-run --runtime LHCb/v42r2 Vetra/v17r0 python VeloAnalysisFramework/scripts/tell1_calib_params_root.py  /path/to/xml_destination/folder  /path/to/root_destination/folder --root --run_list
```
3. Now you can point to specific data by setting the `--calib-data-dir=/path/to/root_destination/folder`


# Basic Developement guide
(0.) Create an issue on gitlab, describe the changes that are needed.
1. `git pull` - update your repository to the newest version aviable
2. Create your own git branch by `git checkout -b name_of_your_branch`
3. Do some changes
4. `git add name_of_changed_file` add files that you want to commit - **be sure that you add only files that you have changed** (use `git status` to see if you added right files)
5. `git commit -m "commit message"` commit the changes. If you are closing an gitlab issue, use gitlab [automatic issue closing](https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html). Be descriptive with the commit message.
6. `git merge origin master` - this is to make sure that that you are up to date with the repository changes
7. `git push origin name_of_your_branch` - you are now pushing the commits to the gitlab repository. Please make sure that you are pushing the right changes.
8. Go to gitlab page and create merge request.

## Useful links
 - https://lhcb.github.io/first-analysis-steps/lhcb-dev.html
 - https://twiki.cern.ch/twiki/bin/view/LHCb/AlignmentDevelopment
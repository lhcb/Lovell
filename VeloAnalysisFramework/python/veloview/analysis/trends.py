# coding=utf-8
"""This module provides functions for trending information"""

from ..core.interface import ValueFunction
from ..utils.rootutils import frac_above_threshold, frac_below_threshold


class Mean(ValueFunction):
    @classmethod
    def calculate(cls, data_hist, params):
        return {"mean": data_hist.GetMean()}
    @classmethod
    def vars(cls):
        return {"mean": float}

class Rms(ValueFunction):
    @classmethod
    def calculate(cls, data_hist, params):
        return {"rms": data_hist.GetRMS()}
    @classmethod
    def vars(cls):
        return {"rms": float}

class MeanRPhi(ValueFunction):
    @classmethod
    def calculate(cls, data_hist, params):
        sumR = nR = sumPhi = nPhi = 0
        for hist_bin in xrange(data_hist.GetSize()):
            if data_hist.IsBinOverflow(hist_bin) or data_hist.IsBinUnderflow(hist_bin):
                continue
            center = data_hist.GetBinCenter(hist_bin)
            if 0 < center < 42:
                sumR += data_hist.GetBinContent(hist_bin)
                nR += 1
            elif 63 < center < 106:
                sumPhi += data_hist.GetBinContent(hist_bin)
                nPhi += 1
        return {"meanR": 1. * sumR / nR, "meanPhi": 1. * sumPhi / nPhi}
    @classmethod
    def vars(cls):
        return {"meanR": float, "meanPhi": float}

class MeanAC(ValueFunction):
    @classmethod
    def calculate(cls, data_hist, params):
        delta = .0000001
        sumA = nA = sumC = nC = 0
        for hist_bin in xrange(data_hist.GetSize()):
            value = data_hist.GetBinContent(hist_bin)
            if data_hist.IsBinOverflow(hist_bin) or data_hist.IsBinUnderflow(hist_bin):
                continue
            if value < -delta:
                sumC += value
                nC += 1
            elif value > delta:
                sumA += value
                nA += 1

        return {"meanA": 1. * sumA / nA, "meanC": 1. * sumC / nC}

    @classmethod
    def vars(cls):
        return {"meanA": float, "meanC": float}

class FractionAbove(ValueFunction):
    @classmethod
    def calculate(cls, data_hist, params):
        maximum = params[0]
        return {"frac_above": frac_above_threshold(data_hist, maximum)}
    @classmethod
    def vars(cls):
        return {"frac_above": float}
class FractionBelow(ValueFunction):
    @classmethod
    def calculate(cls, data_hist, params):
        minimum = params[0]
        return {"frac_below": frac_below_threshold(data_hist, minimum)}
    @classmethod
    def vars(cls):
        return {"frac_below": float}

#class Median(ValueFunction): # Median
#class MPV(ValueFunction): # Most probable value
#class Landau(ValueFunction): # Landau fit parameters

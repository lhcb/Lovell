"""Methods dealing with run view plots."""
import ROOT

from veloview.config import Config
from veloview.config.run_view import run_view_pages
from veloview.core.io import DQDB
from veloview.runview import utils
from veloview.runview.response_formatters import dictionary_formatter
from veloview.tell1.calina_db_conn import CalinaDB
from datetime import datetime

def get_run_plot(name, run, reference=False, formatter=dictionary_formatter,
                 ref_run = None, normalise = False, notify_box = None):
    """Return the formatted object at the plot path in the run file.

    If reference is True, the corresponding plot from the reference file will
    be returned, else the plot from the `run` run will be returned.
    If no reference run can be found for `run`, None is returned.
    Keyword arguments:
    name -- Path within the run file to the plot object.
            A KeyError is raised if name is not found in the run file
    run -- Run number
    reference -- If True, include the reference plot for the given plot and run
    formatter -- Response formatter used to format the TObject
    """
    if reference and ref_run==None:
        try:
            run = utils.reference_run(name, run)
        except ValueError:
            return None

    elif reference: run = ref_run

    # Get the latest run file in the run's directory
    try:
        path = utils.run_file(run)
    except IOError:
        if notify_box != None:
            notify_box.notify("Run file not found for run {0}".format(run), 'run file not found')
        raise IOError("Run file not found for run {0}".format(run))

    # Try to open the file
    f = ROOT.TFile(path)
    if f.IsZombie():
        if notify_box != None:
            notify_box.notify("Run file is zombie for run {0}".format(run), 'is zombie')
        raise IOError("Run file is zombie for run {0}".format(run), 'is zombie')

    # Retrieve the object
    obj = f.Get(name)
    if not obj:
        if notify_box != None:
            msg = "Plot {0} not found in run file {1}"
            msg = msg.format(name, run).rstrip()
            notify_box.notify("Plot {0} not found in run file {1}".format(name, run), 'not found')
        raise KeyError("Plot {0} not found in run file {1}".format(name, run), 'not found')
    # The file will be closed when the function returns, so we need to clone
    # the fetched object outside the file's scope
    ROOT.gROOT.cd()
    clone = obj.Clone(obj.GetName())
    f.Close()

    # Normalise histogram if required
#     plot_dict = get_plot_dictionary(name)
#     if plot_dict is not None and plot_dict.get('normalised', False): normalise = True
    if normalise:
        integral = clone.Integral()
        if integral > 0:
            clone.Scale(1.0/integral)

    return formatter(clone)


def get_run_plot_with_reference(name, run, formatter=dictionary_formatter,
                                ref_run = None, normalise = False, notify_box = None):
    """Return the formatted nominal and reference plots.

    A 2-tuple of two plots is returned:
        1. The nominal plot, as returned by
            get_run_plot(name, run, reference=False, formatter)
        2. The reference plot, as returned by
            get_run_plot(name, run, reference=True, formatter)
    in that order.

    If the reference get_run_plot call returns None, None is returned in place
    of the reference object.
    """
    try:
        nominal = get_run_plot(name, run, reference=False, formatter=formatter,
                           normalise = normalise, notify_box = notify_box)
    except KeyError:
        nominal = None

    try:
        reference = get_run_plot(name, run, reference=True,
                                 formatter=formatter, ref_run = ref_run,
                                 normalise= normalise, notify_box = notify_box)
    except KeyError:
        reference = None
    return nominal, reference


def get_plot_dictionary(name):
    for page in run_view_pages.itervalues():
        if 'plots' not in page:
            continue
        for plot in page['plots']:
            if plot['name'] == name:
                return plot

def get_trending_plot(name, run_range, formatter = dictionary_formatter):
    """
    Get a trending plot, showing a certain variable plotted against run number.
    @param name the name of the variable to plot.
    @param run_range list of all run numbers to plot. If this contains exactly
           two items, it is treated as a range instead, plotting all runs with
           run number greater than or equal to the first item but not greater
           than the second.
    """
    db = DQDB(Config().dq_db_file_path)
    data = db.trend(name, run_range)
    db.close()

    return formatter(dict(name=name, title='run number versus {0}'.format(name), xLabel="run number", yLabel=name, data=data))

def get_2d_trending_plot(nameX, nameY, run_range, formatter = dictionary_formatter):
    """
    Get a trending plot, showing two variables plotted against each other.
    @param nameX the name of the first variable to plot.
    @param nameY the name of the second variable to plot.
    @param run_range list of all run numbers to plot. If this contains exactly
           two items, it is treated as a range instead, plotting all runs with
           run number greater than or equal to the first item but not greater
           than the second.
    """
    db = DQDB(Config().dq_db_file_path)
    data = db.trend2d(nameX, nameY, run_range)
    db.close()

    return formatter(dict(name='{0};{1}'.format(nameX, nameY), title='{0} versus {1}'.format(nameX, nameY), xLabel=nameX, yLabel=nameY, data=data))


def get_special_plot(name, run, reference=False, formatter=dictionary_formatter,
                 ref_run = None, normalise = False, notify_box = None):
    """Return the formatted object at the plot path in the run file.

    """
    import os
    path = os.path.join(Config().run_data_dir,run)
    # Try to open the file
    f = ROOT.TFile(path)
    #if f.IsZombie():
    #    if notify_box != None:
    #        notify_box.notify("Run file is zombie for run {0}".format(run), 'is zombie')
    #    raise IOError("Run file is zombie for run {0}".format(run), 'is zombie')

    # Retrieve the object

    obj = f.Get(name)
    if not obj:
        if notify_box != None:
            msg = "Plot {0} not found in run file {1}"
            msg = msg.format(name, run).rstrip()
            notify_box.notify("Plot {0} not found in run file {1}".format(name, run), 'not found')
        raise KeyError("Plot {0} not found in run file {1}".format(name, run), 'not found')
    # The file will be closed when the function returns, so we need to clone
    # the fetched object outside the file's scope
    ROOT.gROOT.cd()
    clone = obj.Clone(obj.GetName())
    f.Close()

    # Normalise histogram if required
#     plot_dict = get_plot_dictionary(name)
#     if plot_dict is not None and plot_dict.get('normalised', False): normalise = True
    if normalise:
        integral = clone.Integral()
        if integral > 0:
            clone.Scale(1.0/integral)

    return formatter(clone)

def get_calina_data(database_path, plot_type, day=None, sensor_number=None):
    with CalinaDB(database_path, 'calib') as db:
        if plot_type == 'by_day':
            return db.get_by_day(day)
        elif plot_type == 'by_sensor':
            return db.get_by_sensor(day, sensor_number)

def get_calina_stats(database_path, plot_type, task_type='calib', sensor_type='R', sensor_number='#0', **kwargs):
    with CalinaDB(database_path, task_type) as db:
        # @TODO here1
        if plot_type == 'by_day':
            return db.get_stats_trend_by_day(sensor_type=sensor_type, **kwargs)
        elif plot_type == 'by_sensor':
            return db.get_stats_trend_by_sensor(sensor_number=sensor_number, **kwargs)


def get_formatted_calina_data(database_path, plot_type, **kwargs):
    # @TODO here2
    day_stats = get_calina_stats(database_path, plot_type, **kwargs)
    if 'noise' in kwargs['task_type']:
        for i, data in enumerate(day_stats):
            day_stats[i]['label'] = data['runid']
    else:
        for i, data in enumerate(day_stats):
            day_stats[i]['label'] = datetime.strptime(data['day'], "%Y_%m_%d-%H_%M_%S").date()
    day_stats.sort(key=lambda x: x['label'])
    return day_stats


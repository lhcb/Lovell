import sqlite3
from matplotlib.cbook import boxplot_stats
import matplotlib.pyplot as plt
import numpy as np

#from VeloAnalysisFramework.python.veloview.tell1.calina_plots import kdeplot_op


def dict_factory(cursor, row):
	d = {}
	for idx, col in enumerate(cursor.description):
		d[col[0]] = row[idx]
	return d

class QuickDB:
	def __init__(self, dbpath):
		self.dbpath = dbpath
		self.conn = None
		self._cursor = None

	@property
	def cursor(self):
		assert self._cursor != None, "Invalid operation, DataBase not connected."
		return self._cursor

	def connect(self):
		self.conn = sqlite3.connect(self.dbpath)
		self._cursor = self.conn.cursor()

	def __enter__(self):
		self.connect()
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		self.conn.close()
		self.conn = None
		self._cursor = None

	def execute_to_dict(self, query):
		data = []
		for row in self.cursor.execute(query):
			if row == None:
				break
			data.append(dict_factory(self.cursor, row))
		if len(data) == 0:
			raise ValueError("Database query result is empty. Query:[{}]".format(query))
		if len(data) == 1:
			return data[0]
		return data

def execute_score_table(func):
	def fun_wrapper(self, **kwargs):
		query = func(self, **kwargs)
		return self.score_request(query)
	return fun_wrapper

def execute_stats_table(func):
	def fun_wrapper(self, **kwargs):
		query = func(self, **kwargs)
		result = self.execute_to_dict(query)
		for i, d in enumerate(result):
			result[i]['fliers'] = np.fromstring(d['fliers'])
		return result
	return fun_wrapper



class CalinaDB(QuickDB):
	columns=dict(
		stats=['mean', 'iqr', 'cilo', 'cihi', 'whishi', 'whislo', 'fliers', 'q1', 'med', 'q3'],
		calib_by_day = ['day', 'sensor_type'],
		calib_by_sensor = ['day', 'sensor_number'],
		noise_by_day=['runid', 'start', 'end', 'sensor_type'],
		noise_by_sensor=['runid', 'start', 'end', 'sensor_number'],
	)
	queries=dict(
		score_query ='SELECT * FROM {table_name} WHERE day = \'{day}\' AND {condition}'.format,
		stats_query='SELECT {columns}' \
			  ' FROM {stats_table}' \
			  ' INNER JOIN {stats_type}' \
			  ' ON {stats_type}.id = {stats_table}.score_id' \
			  ' WHERE {where_condition}'.format,
		limiting_query = ' ORDER BY {order_column} DESC LIMIT {limit}'.format,
	)
	conditions =dict(
		trend_by_day = 'sensor_type = \'{sensor_type}\''.format,
		trend_by_sensor = 'sensor_number = \'{sensor_number}\''.format,
		runid_range = ' AND runid >= {lower_limit} AND runid <= {upper_limit}'.format
	)

	def __init__(self, dbpath, data_type=''):
		QuickDB.__init__(self, dbpath)
		self.data_type = data_type

	def score_request(self, query):
		result = self.execute_to_dict(query)
		result['score'] = np.fromstring(result['score'])
		return result

	@execute_score_table
	def get_by_day(self, day, sensor_type):
		table_name = '{}_by_day'.format(self.data_type)
		condition = self.conditions['trend_by_day'](sensor_type=sensor_type)
		return self.queries['score_query'](day=day, table_name=table_name, condition=condition)

	@execute_score_table
	def get_by_sensor(self, day, sensor_number):
		table_name = '{}_by_sensor'.format(self.data_type)
		condition = self.conditions['trend_by_sensor'](sensor_number=sensor_number)
		return self.queries['score_query'](day=day, table_name=table_name, condition=condition)

	@execute_stats_table
	def get_stats(self, **kwargs):
		stats_type = kwargs.pop('stats_type')
		table_name = '{}_{}'.format(self.data_type, stats_type)
		where_condition = kwargs.pop('where_condition')
		additional_condition = kwargs.get('additional_condition', None)
		if additional_condition is not None:
			condition_type = additional_condition.get('type')
			where_condition += self.conditions[condition_type](**additional_condition)
		stats_table = 'stats_{}'.format(table_name)
		columns = list(CalinaDB.columns[table_name])
		columns += CalinaDB.columns['stats']
		columns_str = ", ".join(columns)
		stats_query = self.queries['stats_query'](
			columns=columns_str,
			stats_table=stats_table,
			stats_type=table_name,
			where_condition=where_condition
		)
		limited = kwargs.pop('limit', None)
		if limited is not None:
			stats_query += CalinaDB.queries['limiting_query'](**limited)
		return stats_query


	def get_stats_trend_by_day(self, **kwargs):
		stats_type = 'by_day'
		sensor_type = kwargs.pop('sensor_type')
		where_condition = self.conditions['trend_by_day'](sensor_type=sensor_type)
		return self.get_stats(stats_type=stats_type, where_condition=where_condition, **kwargs)

	def get_stats_trend_by_sensor(self, **kwargs):
		stats_type = 'by_sensor'
		sensor_number = kwargs.pop('sensor_number')
		sensor_number = '#{}'.format(sensor_number)
		where_condition = self.conditions['trend_by_sensor'](sensor_number=sensor_number)
		return self.get_stats(stats_type=stats_type, where_condition=where_condition)

	def get_by_link(self, day, sensor_number, link_start, link_end):
		raise NotImplementedError()
		pass


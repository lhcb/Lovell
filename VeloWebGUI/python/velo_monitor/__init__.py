from flask import Blueprint, redirect, url_for
import jobmonitor


# Only export externally useful methods
__all__ = ['create_app', 'wsgi']

# Name of environment variable that specifies folder to write logs to
ENV_LOG_FOLDER = 'LOVELL_VELOWEBGUI_LOG_FOLDER'
ENV_ENABLE_FILE_LOGS = 'LOVELL_VELOWEBGUI_ENABLE_FILE_LOGS'
ENV_ENABLE_EMAIL_LOGS = 'LOVELL_VELOWEBGUI_ENABLE_EMAIL_LOGS'
ENV_DEFAULTS = {
    ENV_LOG_FOLDER: './log',
    ENV_ENABLE_FILE_LOGS: True,
    ENV_ENABLE_EMAIL_LOGS: False
}


def create_app():
    """Create a Flask application deriving from jobmonitor."""
    from velo_monitor.run_view import run_view
    from velo_monitor.velo_view import velo_view
    from velo_monitor.special_analyses import special_analyses
    from velo_monitor.settings import settings
    from velo_monitor.job_resolvers import (
        run_view_resolver,
        trends_resolver
    )
    app = jobmonitor.create_app()
    app.config.from_object('velo_monitor.config')

    if not app.debug:
        add_logging(app)

    example = Blueprint('velo_monitor', __name__,
                        template_folder='templates',
                        static_folder='static',
                        static_url_path='/{0}'.format(__name__))
    app.register_blueprint(example)

    app.register_blueprint(run_view, url_prefix='/run_view')
    app.register_blueprint(velo_view, url_prefix='/velo_view')
    app.register_blueprint(special_analyses, url_prefix='/special_analyses')
    app.register_blueprint(settings, url_prefix='/settings')

    app.add_job_resolver(run_view_resolver)
    app.add_job_resolver(trends_resolver)

    # Redirect the index to the overview page
    # Function is here as I can't think of a nicer place to have it
    @app.route('/')
    def index():
        return redirect(url_for('velo_view.overview'))

    return app


def wsgi(*args, **kwargs):
    """Create an app and pass it arguments from a WSGI server, like gunicorn.

    With this, run with something like `gunicorn velo_monitor:wsgi`.
    """
    return create_app()(*args, **kwargs)


def add_logging(app):
    """Add log-to-file and log-to-mail to a Flask app."""
    import logging
    import os

    env = dict(ENV_DEFAULTS)
    env.update(os.environ)

    # Record to file on WARNING
    log_folder = env[ENV_LOG_FOLDER]
    log_path = os.path.join(log_folder, 'velo_monitor.log')
    file_handler = logging.FileHandler(log_path)
    file_handler.setLevel(logging.WARNING)

    # Send an email to the admins on ERROR
    mail_handler = logging.handlers.SMTPHandler(
        '127.0.0.1',
        'alex.pearce@cern.ch',
        app.config.get('ADMINS'),
        'VELO monitoring app failed'
    )
    mail_handler.setLevel(logging.ERROR)

    # Attach the handlers to the app
    if env[ENV_ENABLE_FILE_LOGS]:
        app.logger.addHandler(file_handler)
    if env[ENV_ENABLE_EMAIL_LOGS]:
        app.logger.addHandler(mail_handler)

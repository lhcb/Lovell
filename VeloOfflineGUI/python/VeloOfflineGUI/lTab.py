import glob
import os
import pickle

from PyQt4.QtCore import Qt
from PyQt4.QtGui import (
    QComboBox,
    QGridLayout,
    QGroupBox,
    QLabel,
    QTabWidget,
    QWidget,
    QCheckBox
)

from lFuncs import setPadding
from mplWidget import mplWidget, PlotWidgetCalibration, PlotWidgetCalina, PlotWidgetTV, PlotWidgetIV
import lInterfaces
from pprint import pprint
import logging

logging.basicConfig(format='%(name)s:[%(levelname)s]: %(message)s')
logger = logging.getLogger('lovellGUI')
logger.setLevel(logging.DEBUG)

def numeric_compare(x, y):
    print x, y
    return int(x[0:6]) - int(y[0:6])

class lTabBase(QWidget):
    PlotWidget = mplWidget

    def __init__(self,  params, parent=None):
        QWidget.__init__(self, parent)
        self.params = params
        self.grid_layout = QGridLayout(self)
        self.plots = []
        self.subpages = []
        if 'plots' in self.params: self.setup_plots()
        elif 'subpages' in self.params: self.setup_subpages()
        setPadding(self.grid_layout)


    def setup_plots(self):
        for plot in self.params['plots']: self.plots.append(self.PlotWidget(plot))
        for i in range(len(self.plots)):
            if 'layout' in self.params: nRows = self.params['layout'][0]
            else: nRows = 2
            row = i/nRows
            col = i%nRows
            self.grid_layout.addWidget(self.plots[i], row, col)

    def setup_subpages(self):
        self.tabs = QTabWidget(self)
        self.grid_layout.addWidget(self.tabs, 0, 0)
        for page in self.params['subpages']:
            subpage = lTab(page, self)
            self.subpages.append(subpage)
            self.tabs.addTab(subpage, page['title'])
        self.tabs.currentChanged.connect(self.parentWidget().tab_changed)

class SimpleTab(lTabBase):
    def replot(self, tab_ops_state, notify_box):
        for plot in self.plots: plot.on_draw(tab_ops_state, notify_box)

class lTabCalibrationParameters(SimpleTab):
    PlotWidget = PlotWidgetCalibration

class lTabCalina(SimpleTab):
    PlotWidget = PlotWidgetCalina

class lTab(lTabBase):
    def replot(self, tab_ops_state, notify_box):
        for plot in self.plots: plot.on_draw(tab_ops_state, notify_box)
        if len(self.subpages) > 0:
            self.subpages[self.tabs.currentIndex()].replot(tab_ops_state, notify_box)


    def replotTrend(self, tab_ops_state):
        for plot in self.plots: plot.on_draw_trend(tab_ops_state)

    def replotSensorTrend(self, tab_ops_state):
        # Retrieve list of trending variables
        self.variableList = sorted(lInterfaces.trending_variables_sensor(tab_ops_state.module_id), reverse = True)
        
        currentItems = []
        for i in xrange(self.variableBox.count()):
            currentItems.append(self.variableBox.itemText(i))
        
        # Don't update if nothing changed
        if currentItems != [x[1] for x in self.variableList]:
            oldIndex = max(0, self.variableBox.currentIndex())
            self.variableBox.blockSignals(True) # No signals when modifying items
            self.variableBox.clear()
            for var in self.variableList:
                self.variableBox.addItem(var[1])
            self.variableBox.blockSignals(False)
            self.variableBox.setCurrentIndex(oldIndex)

        for plot in self.plots: plot.on_draw_trend(tab_ops_state)

    def dataIvFile(self):
        return self.dataIvBox.currentText()
    
    
    def refIvFile(self):
        return self.refIvBox.currentText()


    def is_temp_corrected(self):
        return self.temperature_corr_box.isChecked()

    
    def trendVariable(self):
        index = self.variableBox.currentIndex()
        return self.variableList[index][0]


    def trendVariable2d(self):
        index = self.variableBox2d.currentIndex()
        return self.variableList[index][0]

    
    def is2d(self):
        return self.two_dimension


    def trendStartNum(self):
        return self.runNumStartBox.currentText()

    
    def trendEndNum(self):
        return self.runNumEndBox.currentText()


    def modifyPageForTrending(self, run_data_dir, two_dimension):
        # Used to add options bar for trending plots
        self.run_data_dir = run_data_dir 
        self.two_dimension = two_dimension
        self.variableBox = QComboBox(self)
        self.variableBox.currentIndexChanged.connect(self.parent().tab_options.state_changed)
        self.variableBox2d = QComboBox(self)
        self.variableBox2d.currentIndexChanged.connect(self.parent().tab_options.state_changed)
        # If two dimensional options not set, hide second variable box
        if two_dimension == False:
            self.variableBox2d.setVisible(False)
        self.runNumStartBox = QComboBox(self)
        self.runNumStartBox.currentIndexChanged.connect(self.parent().tab_options.state_changed)
        self.runNumEndBox = QComboBox(self)
        self.runNumEndBox.currentIndexChanged.connect(self.parent().tab_options.state_changed)

        widg = QGroupBox('Trending options', self)
        widgLayout = QGridLayout(widg)
        if two_dimension == True:
            lab1 = QLabel("X trend variable:")
        else:
            lab1 = QLabel("Trend variable:")
        lab1.setAlignment(Qt.AlignRight)
        lab2 = QLabel("Y trend variable:")
        lab2.setAlignment(Qt.AlignRight)
        # If two dimensional options not set, hide second variable label
        if two_dimension == False:
            lab2 = QLabel("")
        lab3 = QLabel("Initial run:")
        lab3.setAlignment(Qt.AlignRight)
        lab4 = QLabel("Final run:")
        lab4.setAlignment(Qt.AlignRight)
        widgLayout.addWidget(lab1, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(self.variableBox, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(lab2, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(self.variableBox2d, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(lab3, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(self.runNumStartBox, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(lab4, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(self.runNumEndBox, 0, widgLayout.columnCount(), 1, 1)
        self.grid_layout.addWidget(widg, self.grid_layout.rowCount(), 0, 1, self.grid_layout.columnCount())

        # Retrieve list of trending variables
        self.variableList = sorted(lInterfaces.trending_variables(), reverse = True)
        
        for var in self.variableList:
            self.variableBox.addItem(var[1])
            self.variableBox2d.addItem(var[1])
        
        runList = lInterfaces.run_list(self.run_data_dir)
        runListSorted = sorted(runList)

        for run in runListSorted:
                self.runNumStartBox.addItem(str(run))
                self.runNumEndBox.addItem(str(run))
        listEntries = len(runListSorted)
        self.runNumEndBox.setCurrentIndex(listEntries-1)

    def modifyPageForSensorTrending(self, run_data_dir):
        # Used to add options bar for trending plots
        self.run_data_dir = run_data_dir 
        self.two_dimension = False
        self.variableBox = QComboBox(self)
        self.variableBox.currentIndexChanged.connect(self.parent().tab_options.state_changed)
        self.runNumStartBox = QComboBox(self)
        self.runNumStartBox.currentIndexChanged.connect(self.parent().tab_options.state_changed)
        self.runNumEndBox = QComboBox(self)
        self.runNumEndBox.currentIndexChanged.connect(self.parent().tab_options.state_changed)

        widg = QGroupBox('Trending options', self)
        widgLayout = QGridLayout(widg)
        lab1 = QLabel("Trend variable:")
        lab1.setAlignment(Qt.AlignRight)
        lab3 = QLabel("Initial run:")
        lab3.setAlignment(Qt.AlignRight)
        lab4 = QLabel("Final run:")
        lab4.setAlignment(Qt.AlignRight)
        widgLayout.addWidget(lab1, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(self.variableBox, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(lab3, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(self.runNumStartBox, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(lab4, 0, widgLayout.columnCount(), 1, 1)
        widgLayout.addWidget(self.runNumEndBox, 0, widgLayout.columnCount(), 1, 1)
        self.grid_layout.addWidget(widg, self.grid_layout.rowCount(), 0, 1, self.grid_layout.columnCount())
        
        runList = lInterfaces.run_list(self.run_data_dir)
        runListSorted = sorted(runList)

        for run in runListSorted:
                self.runNumStartBox.addItem(str(run))
                self.runNumEndBox.addItem(str(run))
        listEntries = len(runListSorted)
        self.runNumEndBox.setCurrentIndex(listEntries-1)


class lTabIV(lTab) : 

    PlotWidget = PlotWidgetIV

    def __init__(self,params, iv_data_dir, temperature_corr = True, parent = None):

        QWidget.__init__(self, parent)
        self.params = params
        self.grid_layout = QGridLayout(self)
        self.plots = []
        self.subpages = []
        if 'plots' in self.params: self.setup_plots()
        elif 'subpages' in self.params: self.setup_subpages()
        setPadding(self.grid_layout)

        # Called after plots are setup.
        self.dataIvBox = QComboBox(self)
        self.dataIvBox.currentIndexChanged.connect(self.parent().tab_options.state_changed)
        self.refIvBox = QComboBox(self)
        self.refIvBox.currentIndexChanged.connect(self.parent().tab_options.state_changed)
        if temperature_corr : 
            self.temperature_corr_box = QCheckBox("Temperature corrected")
            self.temperature_corr_box.clicked.connect(self.parent().tab_options.state_changed)
 
        widg = QGroupBox('IV File Selection', self)
        widgLayout = QGridLayout(widg)
        lab1 = QLabel("Data file:")
        lab1.setAlignment(Qt.AlignRight)
        lab2 = QLabel("Ref file:")
        lab2.setAlignment(Qt.AlignRight)
        widgLayout.addWidget(lab1, 0, 0, 1, 1)
        widgLayout.addWidget(self.dataIvBox, 0, 1, 1, 1)
        widgLayout.addWidget(lab2, 1, 0, 1, 1)
        widgLayout.addWidget(self.refIvBox, 1, 1, 1, 1)
        if temperature_corr : 
            widgLayout.addWidget(self.temperature_corr_box, 0, 2, 1, 1)
        self.grid_layout.addWidget(widg, self.grid_layout.rowCount(), 0, 1, self.grid_layout.columnCount())
        ivNamesDate = lInterfaces.iv_files(iv_data_dir)
        if (len(ivNamesDate)) == 0:
            msg = 'No IV files found'
            self.parent().tab_options.notify(msg, 'No IV files found')
            logger.warning(msg)
       
        SCANS, DATETIMES = zip(*sorted(
            # 2-tuples of (scan name, datetime)
            ivNamesDate,
            # Sort by the second element of the tuple, the datetime
            key=lambda x: x[1],
            # Sort in reverse order, with the newest datetime first
            reverse=True)
        )
         
        for scan in SCANS:
            self.dataIvBox.addItem(scan)
            self.refIvBox.addItem(scan)
        
        fn = os.path.join(os.path.dirname(__file__), 'sensor_mapping.p')
        self.sensor_mapping = pickle.load(open(fn, "rb" ))
        
        self.refIvBox.setCurrentIndex(1)

class lTabTV(lTabIV) : 
    PlotWidget = PlotWidgetTV


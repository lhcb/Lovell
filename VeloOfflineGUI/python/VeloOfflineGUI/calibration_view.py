from collections import OrderedDict

PEDESTALS = (
    'pedestal',{
    'plots':
        [
            {
                'title': "pedestal",
                'name': "pedestal",
                'sensor_dependent': False,
                'y_mean_limits': {
                    'overlay': [-60, 60],  # default, or no overlay,
                    'data_minus_ref': [-10, 10],
                    'data_div_ref': [-0.6, 1.4],
                },
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'Pedestal'
    }
)

HIT_THRESHOLD = (
    'hit_threshold',{
    'plots':
        [
            {
                'title': "hit threshold",
                'name': "hit_threshold",
                'sensor_dependent': False,
                'y_mean_limits': 'auto',
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'Hit threshold'
    }
)


LOW_THRESHOLD = (
    'low_threshold',{
    'plots':
        [
            {
                'title': "low threshold",
                'name': "low_threshold",
                'sensor_dependent': False,
                'y_mean_limits': 'auto',
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'low threshold'
    }
)

OUTLIERNESS_TREND_R = (
    'outlierness_trend_R',{
    'plots':
        [
            {
                'title': "outlierness trend R",
                'name': "outlierness_trend_R",
                'sensor_dependent': False,
                'y_mean_limits': 'auto',
                'calina_db_path': '/calib/velo/dqm/calina/calina/database.db',
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'outlierness trend R'
    }
)



OUTLIERNESS_TREND_phi = (
    'outlierness_trend_sensor',{
    'plots':
        [
            {
                'title': "outlierness trend sensor",
                'name': "outlierness_trend_sensor",
                'sensor_dependent': True,
                'y_mean_limits': 'auto',
                'calina_db_path': '/calib/velo/dqm/calina/calina/database.db',
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'outlierness trend sensor'
    }
)

OUTLIERNESS_TREND_sensor = (
    'outlierness_trend_phi',{
    'plots':
        [
            {
                'title': "outlierness trend phi",
                'name': "outlierness_trend_phi",
                'sensor_dependent': False,
                'y_mean_limits': 'auto',
                'calina_db_path': '/calib/velo/dqm/calina/calina/database.db',
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'outlierness trend phi'
    }
)



OUTLIERNESS_TREND_noise = (
    'outlierness_noise',{
    'plots':
        [
            {
                'title': "outlierness noise",
                'name': "outlierness_noise",
                'sensor_dependent': False,
                'y_mean_limits': 'auto',
                'calina_db_path':  '/calib/velo/dqm/calina/calina/database_noise.db',
                'query_limits': dict(order_column='runid', limit=20),
                'options':
                    {
                        'style': 1,
                        'reference': {  # styles for reference overlay
                            'reference_style': 1,  # not default
                            'reference_color': 'r',  # not default
                        }
                    }
            }
        ],
    'title': 'outlierness noise'
    }
)


special_view_pages = OrderedDict([
    PEDESTALS,
    HIT_THRESHOLD,
    LOW_THRESHOLD,
    OUTLIERNESS_TREND_R,
    OUTLIERNESS_TREND_phi,
    OUTLIERNESS_TREND_sensor,
    OUTLIERNESS_TREND_noise
])

################################################################################
# Package: VeloOfflineGUI
################################################################################
gaudi_subdir(VeloOfflineGUI v3r0)

gaudi_depends_on_subdirs(VeloAnalysisFramework)

find_package(pyanalysis)
find_package(pygraphics)
find_package(PythonLibs)
find_package(pytools)

find_package(Boost COMPONENTS system)

find_package(ROOT COMPONENTS Core RIO)
set(ENV{LD_LIBRARY_PATH} "${std_library_path}:$ENV{LD_LIBRARY_PATH}")
find_package(Qt COMPONENTS QtCore QtGui)
include(${QT_USE_FILE})

QT4_ADD_RESOURCES(RC_SRCS qt_resources/Resources.qrc)

gaudi_install_python_modules()
gaudi_install_scripts()
